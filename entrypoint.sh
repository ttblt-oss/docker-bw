#!/bin/bash

set -e


bw config server ${BW_HOST}

if [ -n "${BW_CLIENTID}" ] && [ -n "${BW_CLIENTSECRET}" ]; then
    bw login --apikey
    export BW_SESSION=$(bw unlock --passwordenv BW_PASSWORD --raw)
else
    export BW_SESSION=$(bw login ${BW_USER} --passwordenv BW_PASSWORD --raw)
    #export BW_SESSION=$(bw unlock --passwordenv BW_PASSWORD --raw)
fi

bw unlock --check

echo 'Running `bw server` on port 8087'
bw serve $@ #--hostname 0.0.0.0 #--disable-origin-protection
